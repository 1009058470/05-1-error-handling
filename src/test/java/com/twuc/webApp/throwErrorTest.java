package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import javax.xml.ws.spi.http.HttpContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
//@AutoConfigureMockMvc

public class throwErrorTest {

    @Autowired
    TestRestTemplate template;
//    @Autowired
//    MockMvc mockMvc;

    @Test
    void test_throwerr_status() throws Exception {
//        mockMvc.perform(get("/api/exceptions/runtime-exception"))
//                .andExpect(status().is5xxServerError());
        ResponseEntity<String> forEntity = template.getForEntity("/api/exceptions/runtime-exception", String.class);
        // assertEquals(forEntity.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
        //template.getForEntity("/api/exceptions/runtime-exception", String.class);
        // HttpStatus.Ba
    }

    @Test
    void test_without_no_exception() {
        ResponseEntity<String> forEntity = template.getForEntity("/api/exceptions/runtime-exception", String.class);
        assertEquals(HttpStatus.NOT_FOUND,forEntity.getStatusCode());

        assertEquals(forEntity.getBody(),"handle");
    }

    @Test
    void test_null_point_exception() {
        ResponseEntity<String> forEntity = template.getForEntity("/api/errors/null-pointer",String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT,forEntity.getStatusCode());
    }

    @Test
    void test_ArithmeticException_exception() {
        ResponseEntity<String> forEntity = template.getForEntity("/api/errors/arithmetic",String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT,forEntity.getStatusCode());
    }

    @Test
    void test_another_to_handle_same_exception_in_different_controller() {
        ResponseEntity<String> forEntitySister = template.getForEntity("/api/sister-errors/illegal-argument",String.class);
        ResponseEntity<String> forEntity = template.getForEntity("/api/brother-errors/illegal-argument",String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT,forEntity.getStatusCode());
        assertEquals(HttpStatus.I_AM_A_TEAPOT,forEntitySister.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON_UTF8, forEntity.getHeaders().getContentType());
        assertEquals(MediaType.APPLICATION_JSON_UTF8, forEntitySister.getHeaders().getContentType());
        assertEquals("{\"message\": \"Something wrong with brother or sister.\"}", forEntity.getBody());
    }


}
