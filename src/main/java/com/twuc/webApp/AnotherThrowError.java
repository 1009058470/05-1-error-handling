package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnotherThrowError {
    @GetMapping("/api/brother-errors/illegal-argument")
    public void sisterErrorIllegError() {
        throw new IllegalArgumentException("IllegeError exception");
    }


}




