package com.twuc.webApp;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;


@RestController
@ControllerAdvice     //设置全局异常注解
public class ThrowError {
    @GetMapping("/api/exceptions/runtime-exception")
    public void toThrowErr(){
        throw new RuntimeException();
    }

    @GetMapping("/api/exceptions/access-control-exception")
    public void toThrowAccessException(){
        throw new AccessControlException("233");
    }

    @GetMapping("/api/errors/null-pointer")
    public void toThrowNullpointException(){
        throw new NullPointerException();
    }

    @GetMapping("/api/errors/arithmetic")
    public void toThrowArthmetic(){
        throw new ArithmeticException();
    }

    @GetMapping("/api/sister-errors/illegal-argument")
    public void has_a_sister_and_illage_exception(){
        throw new IllegalArgumentException();
    }

    @ExceptionHandler
    public ResponseEntity<String> handleIllegeException(IllegalArgumentException illarg){
        return ResponseEntity.status(418).contentType(MediaType.APPLICATION_JSON_UTF8).body("{\"message\": \"Something wrong with brother or sister.\"}");
    }

    @ExceptionHandler
    public ResponseEntity<String> handle(RuntimeException runtime){
        return ResponseEntity.status(404).body("handle");
    }

    @ExceptionHandler
    public  ResponseEntity<String> handleAccess(AccessControlException accExc){
        return ResponseEntity.status(403).body("acc exc");
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<String> handleNullPoint(){
        return ResponseEntity.status(418).body("418");
    }


}
